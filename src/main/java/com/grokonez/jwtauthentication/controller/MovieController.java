package com.grokonez.jwtauthentication.controller;

import com.grokonez.jwtauthentication.exception.ResourceNotFoundException;
import com.grokonez.jwtauthentication.model.Movie;
import com.grokonez.jwtauthentication.repository.MovieRepository;
import com.grokonez.jwtauthentication.security.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/auth")
public class MovieController {
    @Autowired

    private MovieRepository movieRepository;
    @Autowired
    private MovieService movieService;
    @GetMapping("/movies")

    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }


    @RequestMapping(value = "/movies/page/{page}/{size}", method = RequestMethod.GET)
    public List<Movie> getAllByPage(@PathVariable(name = "page") Integer page,
                                    @PathVariable(name = "size") Integer size) {
        return movieService.getAllByPage(page, size);
    }


    @GetMapping("/movies/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable(value = "id") Long id)

            throws ResourceNotFoundException {

        Movie movie = movieRepository.findById(id)

                .orElseThrow(() -> new ResourceNotFoundException("Movie not found for this id :: " + id));

        return ResponseEntity.ok().body(movie);

    }



    @PostMapping("/movies")

    public Movie createMovie(@Valid @RequestBody Movie movie) {

        return movieRepository.save(movie);

    }

    @PutMapping("/movies/update")

    public ResponseEntity<Movie> updateDress(@Valid @RequestBody Movie movieDetails) throws ResourceNotFoundException {

        final Movie updatedMovie = movieRepository.save(movieDetails);

        return ResponseEntity.ok(updatedMovie);

    }

//    @PutMapping("/movies/{id}")
//
//    public ResponseEntity<Movie> updateMovie(@PathVariable(value = "id") Long id,
//
//                                                   @Valid @RequestBody Movie movieDetails) throws ResourceNotFoundException {
//
//        Movie movie = movieRepository.findById(id)
//
//                .orElseThrow(() -> new ResourceNotFoundException("Movie not found for this id :: " + id));
//
//
//
//        movie.setId(movieDetails.getId());
//
//        movie.setTitle(movieDetails.getTitle());
//
//        movie.setCountry(movieDetails.getCountry());
//
//        movie.setLength(movieDetails.getLength());
//
//        movie.setDescription(movieDetails.getDescription());
//
//        movie.setPrice(movieDetails.getPrice());
//
//        final Movie updatedMovie = movieRepository.save(movie);
//
//        return ResponseEntity.ok(updatedMovie);
//
//    }



    @DeleteMapping("/movies/{id}")

    public Map<String, Boolean> deleteMovie(@PathVariable(value = "id") Long id)

            throws ResourceNotFoundException {

        Movie movie = movieRepository.findById(id)

                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));



        movieRepository.delete(movie);

        Map<String, Boolean> response = new HashMap<>();

        response.put("deleted", Boolean.TRUE);

        return response;

    }

}

