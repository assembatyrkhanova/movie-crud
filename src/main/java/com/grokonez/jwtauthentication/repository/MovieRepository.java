package com.grokonez.jwtauthentication.repository;

import com.grokonez.jwtauthentication.model.Movie;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    Movie findByTitle(String title);
    @Query("from Movie")
    List<Movie> getAllByPage(Pageable pageable);
}
