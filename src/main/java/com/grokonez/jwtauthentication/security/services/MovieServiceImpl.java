package com.grokonez.jwtauthentication.security.services;

import com.grokonez.jwtauthentication.model.Movie;
import com.grokonez.jwtauthentication.model.MovieDto;
import com.grokonez.jwtauthentication.repository.MovieRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service(value = "movieService")
public class MovieServiceImpl implements  MovieService {

    @Autowired
    private MovieRepository movieDao;


    public List<Movie> findAll() {
        List<Movie> list = new ArrayList<>();
        movieDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(Long id) {
        movieDao.deleteById(id);
    }

    @Override
    public Movie findOne(String title) {
        return movieDao.findByTitle(title);
    }

    @Override
    public Movie findById(Long id) {
        Optional<Movie> optionalMovie = movieDao.findById(id);
        return optionalMovie.isPresent() ? optionalMovie.get() : null;
    }

    @Override
    public MovieDto update(MovieDto movieDto) {
        Movie movie = findById(movieDto.getId());
        if(movie != null) {
            BeanUtils.copyProperties(movieDto, movie);
            movieDao.save(movie);
        }
        return movieDto;
    }
    @Override
    public List<Movie> getAllByPage(int page, int size) {
        return movieDao.getAllByPage(new PageRequest(page, size));
    }

    @Override
    public Movie save(MovieDto movie) {
        Movie newMovie = new Movie();
        newMovie.setTitle(movie.getTitle());
        newMovie.setCountry(movie.getCountry());
        newMovie.setLength(movie.getLength());
        newMovie.setDescription(movie.getDescription());
        return movieDao.save(newMovie);
    }


}
