package com.grokonez.jwtauthentication.security.services;

import com.grokonez.jwtauthentication.model.Movie;
import com.grokonez.jwtauthentication.model.MovieDto;

import java.util.List;

public interface MovieService {
    Movie save(MovieDto movie);
    List<Movie> findAll();
    void delete(Long id);
    List<Movie> getAllByPage(int page, int size);

    Movie findOne(String title);

    Movie findById(Long id);

    MovieDto update(MovieDto movieDto);
}
